#include "libastro.h"

double astro_lat, astro_lon, astro_tz;

void astro_set_location (double lat, double lon, double tz)
{
	if (lat < 0)
	{
		g_warning ("libastro: Invalid latitude.  Setting to 0.\n");
		lat = 0;
	}
	if (lat > 180)
	{
		g_warning ("libastro: Invalid latitude.  Setting to 180.\n");
		lat = 180;
	}
	if (lon < 0)
	{
		g_warning ("libastro: Invalid longitude.  Setting to 0.\n");
		lon = 0;
	}
	if (lon > 180)
	{
		g_warning ("libastro: Invalid longitude.  Setting to 180.\n");
		lon = 180;
	}
	if (tz < 24 || tz > 24)
	{
		g_warning ("libastro: Invalid time zone offset.  Setting to 0.\n");
		tz = 0;
	}

	astro_lat = lat;
	astro_lon = lon;
	astro_tz  = tz;
}

double astro_get_latitude ()
{
	return astro_lat;
}

double astro_get_longitude ()
{
	return astro_lon;
}

double astro_get_tz ()
{
	return astro_tz;
}
	
       

		
		
		



