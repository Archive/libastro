/*
  libastro - A simple astronomical calculation library
  Copyright (C) Russell Steinthal 1999

  See COPYING for license terms (GPL)
*/

#include <glib.h>

void astro_set_location (double latitude, double longitude, double tz);
double astro_get_latitude ();
double astro_get_longitude ();
double astro_get_tz ();

void astro_get_sunrise (int year, int month, int day);
void astro_get_sunset (int year, int month, int day);

extern astro_lat, astro_lon, astro_tz;

